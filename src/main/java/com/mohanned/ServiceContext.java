package com.mohanned;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class ServiceContext extends ServerResource {

    @Get
    public String getServiceContext() {
       
        String FullContext = null;
FullContext= "        <Preferences>\n" +
"            <noPreferenceRules type=\"http://www.w3.org/2001/XMLSchema#boolean\">false</noPreferenceRules>\n" +
"            <hasProperty>\n" +
"                <ServicePriority>\n" +
"                    <propertyValue type=\"http://www.w3.org/2001/XMLSchema#int\">50</propertyValue>\n" +
"                    <propertyType type=\"http://www.w3.org/2001/XMLSchema#string\">INT</propertyType>\n" +
"                    <criteria type=\"http://www.w3.org/2001/XMLSchema#string\">http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#ServicePriorityCriteria</criteria>\n" +
"                </ServicePriority>\n" +
"            </hasProperty>\n" +
"            <rules>\n" +
"                <rule>[SampleServicePreference1a: (?service rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#MigratableService), (?origin rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#CandidateOriginServiceProvider), (?destination rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#CandidateDestinationServiceProvider), (?origin http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#provides http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#service1), (?destination http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#hasProperty ?property), (?property rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#FreeMemory),          (?property http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#propertyValue ?v1), ge(?v1, \"2048\"^^http://www.w3.org/2001/XMLSchema#int) -> (http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#service1 http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#possibleDestinationProvider ?destination)]</rule>\n" +
"                <rule>[SampleServicePreference1b: (?service rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#MigratableService), (?origin rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#CandidateOriginServiceProvider), (?destination rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#CandidateDestinationServiceProvider), (?origin http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#provides http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#service1), (?destination http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#hasProperty ?property), (?property rdf:type http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#PermanentStorageSize),(?property http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#propertyValue ?v1), ge(?v1, \"2048\"^^http://www.w3.org/2001/XMLSchema#int) -> (http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#service1 http://www.fit.vutbr.cz/homes/rychly/WSMF/Core#possibleDestinationProvider ?destination)]</rule>\n" +
"            </rules>\n" +
"        </Preferences>";
        
        return FullContext;
    }
}
