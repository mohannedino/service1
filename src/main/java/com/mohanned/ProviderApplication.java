package com.mohanned;

import org.restlet.Application;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.ext.wadl.ApplicationInfo;
import org.restlet.ext.wadl.DocumentationInfo;
import org.restlet.ext.wadl.WadlApplication;
import org.restlet.representation.Representation;
import org.restlet.resource.Directory;
import org.restlet.routing.Router;

public class ProviderApplication extends WadlApplication {

    /**
     * Creates a root Restlet that will receive all incoming calls.
     * @return router
     */
    @Override
    public synchronized Restlet createInboundRoot() {
        Router router = new Router(getContext());

        // Defines only one route
        router.attachDefault(new Directory(getContext(), "war:///"));
        router.attach("/who", Who.class);
        router.attach("/getServiceContext", ServiceContext.class);
        return router;
    }

    @Override
    public ApplicationInfo getApplicationInfo(Request request, Response response) {
        ApplicationInfo result = super.getApplicationInfo(request, response);

        DocumentationInfo docInfo = new DocumentationInfo(
                "Service1 Semantic Description.");
        docInfo.setTitle("Service1 WADL File");
        result.setDocumentation(docInfo);

        return result;
    }
}
